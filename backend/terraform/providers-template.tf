terraform {
  backend "s3" {
    # bucket needs to be hardcoded. See aws s3 ls which bucket name to use.
    bucket         = "terraform-state-${ENVIRONMENT}-2tt4"
    key            = "mosar_flashcard.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "${LOCK_TABLE_FLASHCARD}"
    encrypt        = true
  }

  required_version = "~> 1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

