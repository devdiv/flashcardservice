{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["secretsmanager:*"],
      "Resource": ["arn:aws:secretsmanager:eu-central-1:*:secret:*"]
    },
    {
      "Effect": "Allow",
      "Action": ["logs:CreateLogStream", "logs:PutLogEvents"],
      "Resource": "*"
    },
    {
      "Action": ["rds-db:connect"],
      "Effect": "Allow",
      "Resource": ["arn:aws:rds-db:eu-central-1:*:dbuser:*"]
    },
    {
      "Action": ["rds:*"],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:rds:eu-central-1:640753244498:db:${DB_ID}/${DB_USER}"
      ]
    }
  ]
}

