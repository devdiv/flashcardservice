# flashcard app /main.tf

module "ecs-app" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-ecs-app.git?ref=tags/v1.0.6"
  for_each    = local.apps
  app         = each.value
  environment = var.environment
  managed_by  = var.managed_by
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}

module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.1.2"
  roles       = local.roles
  environment = var.environment
  managed_by  = var.managed_by
}
