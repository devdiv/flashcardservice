# game app / outputs

output "ecs-app" {
  value     = module.ecs-app
  sensitive = true
}

output "iam" {
  value     = module.iam
  sensitive = true
}

output "security_groups" {
  value = module.security_groups
}
