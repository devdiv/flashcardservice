locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  roles = {
    flashcard_task = {
      role = {
        name               = "flashcard-ecs-task-role-${var.environment}"
        description        = "flashcard role tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "flashcard-ecs-task-policy-${var.environment}"
        description = "Policy for Game task role"
        policy = templatefile("./iam_spec_files/flashcard_ecs_task_policy.tpl", {
          DB_USER = local.db.DB_USER
          DB_ID   = local.db.DB_ID
        })
      }
      policy_attachment = {
        name = "flashcard-ecs-task-policy-attachment"
      }
    }
    flashcard_task_execution = {
      role = {
        name               = "flashcard-ecs-task-execution-role-${var.environment}"
        description        = "role to execute the flashcard ecs tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "flashcard-ecs-task-execution-policy-${var.environment}"
        description = "Policy to execute ecs flashcard tasks"
        policy      = file("${path.root}/iam_spec_files/flashcard_ecs_task_execution_policy.json")
      }
      policy_attachment = {
        name = "flashcard-ecs-task-execution-policy-attachment"
      }
    }
  }
}

locals {
  security_groups = {
    flashcard = {
      name        = "flashcard_sg"
      vpc_id      = data.aws_vpc.mosar.id
      description = "security group access to the flashcard service"
      ingress_sg  = {}
      ingress_cidr = {
        http = {
          from        = 8181
          to          = 8181
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  flashcard_names = {
    name           = "flashcards"
    container_name = "flashcard-${var.environment}"
    namespace_id   = regex("/(?P<id>.*)", data.aws_route53_zone.zone.linked_service_description).id
    namespace_name = data.aws_route53_zone.zone.name
  }
}


locals {
  apps = {
    flashcard = {
      name                    = "mosar-flashcard"
      desired_count           = 1
      container_definitions   = templatefile("./mosar-flashcard-container.tpl", local.container_vars)
      subnet_ids              = local.subnets.private
      security_groups         = module.security_groups.security_groups["flashcard"].*.id
      assign_public_ip        = false
      family                  = "mosar-flashcard-family"
      use_load_balancer       = false
      lb_target_group_arn     = ""
      lb_container_name       = ""
      lb_container_port       = ""
      ecs_task_execution_role = module.iam.role["flashcard_task_execution"]
      ecs_task_role           = module.iam.role["flashcard_task"]
      cpu                     = 256
      memory                  = 512
      cluster                 = data.aws_ecs_cluster.cluster
      discovery_service = {
        name              = local.flashcard_names.name
        namespace_id      = local.flashcard_names.namespace_id
        ttl               = 30
        failure_threshold = 5
      }
      autoscaling = {
        max_capacity = 3
        min_capacity = 1
      }
      autoscaling_policies = {
        memory = {
          name                   = "flashcard_memory_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageMemoryUtilization"
          target_value           = 80
        }
        cpu = {
          name                   = "flashcard_cpu_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageCPUUtilization"
          target_value           = 60
        }
      }
    }
  }
}

locals {
  container_vars = {
    SPRING_PROFILES_ACTIVE = var.spring_profiles_active
    FLASHCARD_IMAGE        = var.flashcard_image
    FLASHCARD_IMAGE_TAG    = var.flashcard_image_tag
    ENVIRONMENT            = var.environment
    MOSAR_LOG_GROUP_NAME   = "mosar-logs-${var.environment}"
    DATABASE               = local.db.DATABASE
    DB_HOST                = local.db.DB_HOST
    DB_USER                = local.db.DB_USER
    TRUSTSTORE_PASS        = data.aws_secretsmanager_secret_version.truststore_pass.secret_string
  }
}

locals {
  db = {
    DATABASE = data.aws_db_instance.database.db_name
    DB_HOST  = data.aws_db_instance.database.address
    DB_USER  = data.aws_secretsmanager_secret_version.db_user.secret_string
    DB_ID    = data.aws_db_instance.database.db_instance_identifier
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}

locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}

