# flashcard service / datasources

data "aws_region" "current" {}

data "aws_ecs_cluster" "cluster" {
  cluster_name = "mosar-ecs-cluster-${var.environment}"
}

# TODO: use this when this ticket is resolved: https://github.com/hashicorp/terraform-provider-aws/issues/20313
# data "aws_service_discovery_dns_namespace" "namespace" {
#   name = "mosar.local"
#   type = "DNS_PRIVATE"
# }

# TODO: when above is used, remove this workaround:
data "aws_route53_zone" "zone" {
  name   = "mosar.local"
  vpc_id = data.aws_vpc.mosar.id
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  dynamic "filter" {
    for_each = local.public_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "public" {
  for_each = toset(data.aws_subnets.public.ids)
  id       = each.value
}


data "aws_vpc" "mosar" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_ecr_repository" "mosar_flashcard" {
  name = "mosar-flashcard"
}

data "aws_db_instance" "database" {
  db_instance_identifier = "mosar-db-${var.environment}"
}

data "aws_secretsmanager_secret" "db_user" {
  name = var.environment == "test" ? "db-username1" : "db-username_prod"
}

data "aws_secretsmanager_secret_version" "db_user" {
  secret_id = data.aws_secretsmanager_secret.db_user.id
}

data "aws_secretsmanager_secret" "truststore_pass" {
  name = "truststore_pass"
}

data "aws_secretsmanager_secret_version" "truststore_pass" {
  secret_id = data.aws_secretsmanager_secret.truststore_pass.id
}
