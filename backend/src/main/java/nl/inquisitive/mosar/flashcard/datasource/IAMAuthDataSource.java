package nl.inquisitive.mosar.flashcard.datasource;

import org.springframework.context.annotation.PropertySource;

import java.sql.Connection;
import java.sql.SQLException;
import com.mysql.cj.jdbc.MysqlDataSource;
import lombok.extern.slf4j.Slf4j;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.DefaultAwsRegionProviderChain;
import com.amazonaws.services.rds.auth.GetIamAuthTokenRequest;
import com.amazonaws.services.rds.auth.RdsIamAuthTokenGenerator;

@Slf4j
@PropertySource("classpath:application-${spring.profiles.active}.properties")
public class IAMAuthDataSource
    extends MysqlDataSource {
  private final static long serialVersionUID = 1L;

  @Override
  public Connection getConnection(String user, String password)
      throws SQLException {

    // adapted from:
    // https://chariotsolutions.com/blog/post/rds-database-authentication-with-spring-boot-part-2-iam-authentication/
    RdsIamAuthTokenGenerator generator = RdsIamAuthTokenGenerator.builder()
        .credentials(new DefaultAWSCredentialsProviderChain())
        .region((new DefaultAwsRegionProviderChain()).getRegion())
        .build();

    GetIamAuthTokenRequest request = GetIamAuthTokenRequest.builder()
        .hostname(getServerName())
        .port(getPortNumber())
        .userName(user)
        .build();

    String authToken = generator.getAuthToken(request);

    return super.getConnection(user, authToken);
  }
}
