package nl.inquisitive.mosar.flashcard.exceptions.featureflag;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class FlagNotUniqueException
        extends RuntimeException {

    public FlagNotUniqueException(String flag) {
        super("Feature flag " + flag + " has no unique description");
    }
}

