package nl.inquisitive.mosar.flashcard.repository.courserepository;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@MappedSuperclass
public abstract class AbstractCourseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long courseid;

    @Lob
    @Column(nullable = true)
    @Getter
    @Setter
    private String coursename;
}
