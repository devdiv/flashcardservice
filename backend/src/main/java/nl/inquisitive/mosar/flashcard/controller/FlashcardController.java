package nl.inquisitive.mosar.flashcard.controller;

import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.flashcard.service.FlashcardServiceLayer;
import nl.inquisitive.mosar.flashcard.controller.api.FlashcardApiDelegate;
import nl.inquisitive.mosar.flashcard.controller.model.Course;
import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
import nl.inquisitive.mosar.flashcard.controller.model.Maxflashcards;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import org.modelmapper.ModelMapper;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
@Primary
public class FlashcardController implements FlashcardApiDelegate {

  @Autowired
  FlashcardServiceLayer flashcardServiceLayer;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public ResponseEntity<Flashcard> getFlashcard(String courseid, String flashcardId) {
    log.debug("Entering getFlashcard with ID" + flashcardId);
    // Get one card with ID from database if it exists
    Flashcard flashcard = flashcardServiceLayer.getAFlashcardByCourseIdAndFlashcardId(courseid, flashcardId);
    log.debug("Returning OK for ID " + flashcardId);
    return ResponseEntity.ok().body(flashcard);
  }

  @Override
  public ResponseEntity<List<Course>> getCoursesAvailable() {
    log.debug("Entering getCoursesAvailable");
    List<Course> listofAvailableCoursesResponseEntity = flashcardServiceLayer.listOfAllAvailableCourses().stream()
        .map(course -> modelMapper.map(course, Course.class)).collect(Collectors.toList());
    log.debug("Returning OK for Courses available");
    return ResponseEntity.ok().body(listofAvailableCoursesResponseEntity);
  }

  @Override
  public ResponseEntity<Maxflashcards> getMaxAvailable(String courseid) {
    log.debug("Starting getMaxValue");
    Maxflashcards maxflashcards = flashcardServiceLayer.getMaxValueFromDatabase(courseid);
    log.debug("Returning OK for courseid " + courseid);
    return ResponseEntity.ok().body(maxflashcards);
  }

  /* This is the post for creating flashcards. Disabled due to safety reasons.
   @Override
   public ResponseEntity<Flashcard> createFlashcard(Flashcard flashcard) {

   FlashcardEntity flashcardEntity = FlashcardMapper.INSTANCE.mapTo(flashcard);
   FlashcardEntity savedFlashcard = flashcardsRepository.save(flashcardEntity);

   URI locationForSavedFlashcard =
   ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedFlashcard.getId()).toUri();

   return ResponseEntity.created(locationForSavedFlashcard).build();
   }
  */
}
