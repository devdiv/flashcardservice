package nl.inquisitive.mosar.flashcard.helpers;


import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.flashcard.helpers.readandstripcsv.ReadAndStripCsv;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.ReadPSMJson;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.StripPSMJson;

import java.io.IOException;
import java.net.URISyntaxException;

@Slf4j
public class CreateSQLSeedingFile {
    public static void main(String[] args) throws URISyntaxException {
        log.debug("Starting StripFilesToSQLDataFile");

        StripPSMJson stripPSMJson =  new StripPSMJson(new ReadPSMJson(CreateSQLSeedingFile.class.getResource("/PSM1.json").toURI()).getExamModel());
        //pdf to text  //strip pdf

        //strip csv file into sql file
        ReadAndStripCsv readAndStripCsvDataOps = new ReadAndStripCsv(CreateSQLSeedingFile.class.getResource("/").getPath() + "DataOps.csv");
        ReadAndStripCsv readAndStripCsvIstqb = new ReadAndStripCsv(CreateSQLSeedingFile.class.getResource("/").getPath() + "istqb_foundation.csv");


        //pupulatdatabase

        new nl.inquisitive.mosar.flashcard.helpers.sqlseed.CreateSQLCourse();
        new nl.inquisitive.mosar.flashcard.helpers.sqlseed.CreateSQLSeedingFile(1,readAndStripCsvIstqb.getAllTermsList(), readAndStripCsvIstqb.getAllExplanationsList());
        new nl.inquisitive.mosar.flashcard.helpers.sqlseed.CreateSQLSeedingFile(2,stripPSMJson.getAllTermsList(),stripPSMJson.getAllExplanationsList());
        new nl.inquisitive.mosar.flashcard.helpers.sqlseed.CreateSQLSeedingFile(3,readAndStripCsvDataOps.getAllTermsList(), readAndStripCsvDataOps.getAllExplanationsList());


        log.debug("End StripFilesToSQLDataFile");
    }
}
