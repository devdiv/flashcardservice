package nl.inquisitive.mosar.flashcard.repository.flashcardrepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntitySmall;

@AllArgsConstructor
@Data
public class FlashcardDTO {

    long id;
    String flashcardid;
    CourseEntitySmall courseEntitySmall;
    String term;
    String explanation;
    String createdate;
    String createdby;
    String changedby;
}
