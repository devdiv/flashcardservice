package nl.inquisitive.mosar.flashcard.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "ssl-params")
@Data
public class SslParams {

  // default values, can be override by external settings
  public String trustStorePath = "config/client-truststore.jks";
  public String trustStorePassword = "wso2carbon";
  public String defaultType = "JKS";
}
