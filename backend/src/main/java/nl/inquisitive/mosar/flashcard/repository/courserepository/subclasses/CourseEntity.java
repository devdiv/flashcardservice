package nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses;


import lombok.Getter;
import lombok.Setter;
import nl.inquisitive.mosar.flashcard.repository.courserepository.AbstractCourseEntity;
import nl.inquisitive.mosar.flashcard.repository.flashcardrepository.FlashcardEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Entity
@Table(name = "course_entity")
public class CourseEntity extends AbstractCourseEntity implements Serializable {
//TODO This is for by directional search. At this moment this isn't used and it's all in CourseEntitySmall. (for later)

    @Setter
    @Getter
    @OneToMany
    @JoinColumn(name="courseid",referencedColumnName="courseid")
    @Column(name = "flashcards")
    private Set<FlashcardEntity> flashcards;
}
