package nl.inquisitive.mosar.flashcard.repository.flashcardrepository;

import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntitySmall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FlashcardsRepository extends JpaRepository<FlashcardEntity, Long> {

    @Query(value = "SELECT count(id) FROM flashcards.flashcard_entity WHERE courseid = :courseid", nativeQuery = true)
    int getMaxValueFromDatabase(@Param("courseid") String courseid);

    FlashcardDTO findByCourseEntitySmallAndFlashcardid(CourseEntitySmall courseid, String flashcardId);
}
