package nl.inquisitive.mosar.flashcard.helpers.sqlseed;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Slf4j
public class CreateSQLCourse {


    public CreateSQLCourse() {
        //Information on the SQL file

        String sqlBasicInputStatementStringISTQB = "INSERT INTO `flashcards`.`course_entity`(`coursename`) VALUES (\"ISTQB\")";
        String sqlBasicInputStatementStringPSM1 = "INSERT INTO `flashcards`.`course_entity`(`coursename`) VALUES (\"PSM1\")";
        String sqlBasicInputStatementStringDataOps = "INSERT INTO `flashcards`.`course_entity`(`coursename`) VALUES (\"DATAOPS\")";

        String sqlDataFileName = getClass().getResource("/").getPath() + "data.sql";

        //create file
        try {
            //create file if not exists
            log.debug("Starting file writing.");
            File sqlDataFile = new File(sqlDataFileName);
            if (sqlDataFile.createNewFile()) {
                log.debug("File created: " + sqlDataFile.getName());
            } else {
                log.debug("File already exists. Thus will not be created.");
            }

            //Write SQL statements ot file
            FileWriter sqlDataFileWriter = new FileWriter(sqlDataFileName, true);

            sqlDataFileWriter.write(sqlBasicInputStatementStringISTQB + "\n");
            sqlDataFileWriter.write(sqlBasicInputStatementStringPSM1 + "\n");
            sqlDataFileWriter.write(sqlBasicInputStatementStringDataOps + "\n");


            sqlDataFileWriter.close();
            log.debug("Successfully wrote to the file.");

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}

