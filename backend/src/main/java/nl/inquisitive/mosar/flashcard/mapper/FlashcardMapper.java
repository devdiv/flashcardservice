//package nl.inquisitive.mosar.flashcard.mapper;
//
//
//import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
//import nl.inquisitive.mosar.flashcard.repository.flashcardrepository.FlashcardEntity;
//import org.mapstruct.InheritInverseConfiguration;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.Mappings;
//import org.mapstruct.factory.Mappers;
//
//@Mapper
//public interface FlashcardMapper {
//
//    FlashcardMapper INSTANCE = Mappers.getMapper(FlashcardMapper.class);
//
//    //Mapp the data model from the open API specs on the database specs
//    @Mappings({
//            @Mapping(target = "copursenumber", source = "coursenumber"),
//            @Mapping(target = "flashcardid", source = "flashcardid"),
//            @Mapping(target = "term", source = "term"),
//            @Mapping(target = "explanation", source = "explanation"),
//            @Mapping(target = "createdate", source = "createDate"),
//            @Mapping(target = "createdby", source = "createdBy"),
//            @Mapping(target = "changedby", source = "changedBy"),
//    })
//    FlashcardEntity mapTo(Flashcard flashcard);
//
//    @InheritInverseConfiguration
//    Flashcard mapTo(FlashcardEntity flashcardEntity);
//}
