package nl.inquisitive.mosar.flashcard.helpers.readandstripjson;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.Model.ExamModel;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.Model.QuestionModel;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class StripPSMJson {

    @Setter
    @Getter
    ExamModel examModel;

    @Setter
    @Getter
    List<String> allTermsList;

    @Getter
    @Setter
    List<String> allExplanationsList;

    public StripPSMJson(ExamModel examModel) {
        this.examModel = examModel;
        JsonToRawString();
    }

    public void JsonToRawString(){
        List<String> questionsConversionToTermsList = new ArrayList<>();
        List<String> answersConversionToExplanationsList = new ArrayList<>();
        String allAnswersConcat = "";


        for (QuestionModel questionNumber : examModel.getQuestions()) {
         questionsConversionToTermsList.add(questionNumber.getQuestion());

         for (int numberOfRightAnswers = 0; numberOfRightAnswers < questionNumber.getCorrectAnswer().size(); numberOfRightAnswers++) {
             allAnswersConcat = allAnswersConcat + questionNumber.getAnswers().get(questionNumber.getCorrectAnswer().get(numberOfRightAnswers));

             if (numberOfRightAnswers != questionNumber.getCorrectAnswer().size()-1){
                 allAnswersConcat = allAnswersConcat + ". ";
             }
         }
            answersConversionToExplanationsList.add(allAnswersConcat);
            allAnswersConcat = "";
        }

        setAllTermsList(questionsConversionToTermsList);
        setAllExplanationsList(answersConversionToExplanationsList);
    }
}
