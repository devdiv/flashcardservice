package nl.inquisitive.mosar.flashcard.models;

import lombok.Data;

@Data
public class PostEvaluateFeatureFlagResponse {
    private EvalContext evalContext;
    private EvalDebugLog evalDebugLog;
    private int flagID;
    private String flagKey;
    private int flagSnapshotID;
    private int segmentID;
    private String timestamp;
    private Object variantAttachment;
    private int variantID;
    private String variantKey;
}
