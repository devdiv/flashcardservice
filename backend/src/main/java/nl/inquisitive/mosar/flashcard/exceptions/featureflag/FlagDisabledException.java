package nl.inquisitive.mosar.flashcard.exceptions.featureflag;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class FlagDisabledException
        extends RuntimeException {

    public FlagDisabledException(String flag) {
        super("Feature Flag " + flag + " is disabled");
    }
}
