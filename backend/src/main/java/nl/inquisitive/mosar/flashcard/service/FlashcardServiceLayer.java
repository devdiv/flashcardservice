package nl.inquisitive.mosar.flashcard.service;

import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
import nl.inquisitive.mosar.flashcard.controller.model.Maxflashcards;
import nl.inquisitive.mosar.flashcard.models.PostEvaluateFeatureFlagResponse;
import nl.inquisitive.mosar.flashcard.repository.courserepository.CourseRepositorySmall;
import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntitySmall;
import nl.inquisitive.mosar.flashcard.repository.flashcardrepository.FlashcardDTO;
import nl.inquisitive.mosar.flashcard.repository.flashcardrepository.FlashcardsRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityNotFoundException;

@Component
@Slf4j
public class FlashcardServiceLayer {
  @Value("${feature-flag.psm}")
  private String PSM;

  @Value("${feature-flag.psm.enabled}")
  private String PSM_ENABLED;

  @Value("${feature-flag.psm.disabled}")
  private String PSM_DISABLED;

  @Autowired
  FlashcardsRepository flashcardsRepository;

  @Autowired
  CourseRepositorySmall courseRepositorySmall;

  @Autowired
  FeatureFlagService featureFlagService;

  public List<CourseEntitySmall> listOfAllAvailableCourses() {
    List<CourseEntitySmall> courses = courseRepositorySmall.findAll();
    PostEvaluateFeatureFlagResponse featureFlag = featureFlagService.evaluateFeatureFlag(PSM);
    if (featureFlag != null && featureFlag.getVariantKey().equals(PSM_DISABLED)) {
      courses = courses.stream().filter(course -> course.getCourseid() != 2).collect(Collectors.toList());
    }
    return courses;
  }

  public Flashcard getAFlashcardByCourseIdAndFlashcardId(String courseid, String flashcardId) {
    Flashcard flashcard = new Flashcard();
    FlashcardDTO flashcardDTO = getAFlashcardByCourseIdAndFlashcardIdInDto(courseid, flashcardId);
    BeanUtils.copyProperties(flashcardDTO, flashcard);
    // BeanUtils doesn't copy Courseid because it's not a primitive.
    flashcard.setCourseid((Arrays.asList(flashcardDTO.getCourseEntitySmall().getCourseid(),
        flashcardDTO.getCourseEntitySmall().getCoursename())));
    return flashcard;
  }

  public FlashcardDTO getAFlashcardByCourseIdAndFlashcardIdInDto(String courseid, String flashcardId) {
    CourseEntitySmall courseEntity = courseRepositorySmall.findByCourseid(Long.parseLong(courseid));

    return flashcardsRepository.findByCourseEntitySmallAndFlashcardid(courseEntity, (flashcardId));
  }

  public Maxflashcards getMaxValueFromDatabase(String courseid) {
    int maxflashcards = flashcardsRepository.getMaxValueFromDatabase(courseid);
    if (maxflashcards == 0) {
      throw new EntityNotFoundException("Zero flashcards found");}
    Maxflashcards maxvalue = new Maxflashcards();
    maxvalue.setMaxFlashcards(maxflashcards);
    return maxvalue;
  }
}
