package nl.inquisitive.mosar.flashcard.helpers.readandstripjson.Model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ExamInfoModel {
    private String examName;
    private int numberOfQuestionsInExam;
    private double passPercentage;

    private int amountOfCorrectAnswers;
    private double percentageScored;
    private boolean userPassedExam;

    private Date dateTimeOfExam;
}
