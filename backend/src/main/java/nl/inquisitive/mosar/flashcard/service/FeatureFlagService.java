package nl.inquisitive.mosar.flashcard.service;

import nl.inquisitive.mosar.flashcard.models.FeatureFlagPostEntity;
import nl.inquisitive.mosar.flashcard.models.GetAllFeatureFlagsResponse;
import nl.inquisitive.mosar.flashcard.models.PostEvaluateFeatureFlagResponse;
import nl.inquisitive.mosar.flashcard.exceptions.featureflag.FlagDisabledException;
import nl.inquisitive.mosar.flashcard.exceptions.featureflag.FlagNotFoundException;
import nl.inquisitive.mosar.flashcard.exceptions.featureflag.FlagNotUniqueException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
public class FeatureFlagService {

  @Autowired
  RestTemplate restTemplate;

  @Value("${url.feature_flags}")
  String URL_FEATURE_FLAGS;

  private final String FEATURE_FLAGS_EVALUATION = "/api/v1/evaluation";
  private final String FEATURE_FLAGS_GET_ALL_FLAGS = "/api/v1/flags";

  public PostEvaluateFeatureFlagResponse evaluateFeatureFlag(String flag) {
    log.debug("Entering evaluateFeatureFlag for flag: " + flag);

    // get a list of all available feature flags
    ResponseEntity<GetAllFeatureFlagsResponse[]> getAllFeatureFlagsResponse = restTemplate.getForEntity(this.URL_FEATURE_FLAGS + FEATURE_FLAGS_GET_ALL_FLAGS, GetAllFeatureFlagsResponse[].class);
    List<GetAllFeatureFlagsResponse> allFeatureFlags = Arrays.asList(getAllFeatureFlagsResponse.getBody());
    log.debug("all feature flags as received from api/v1/flags: " + allFeatureFlags.toString());
    List<GetAllFeatureFlagsResponse> filteredFeatureFlags = allFeatureFlags.stream()
        .filter(x -> x.getDescription().equals(flag)).collect(Collectors.toList());

    log.debug("filtered feature flags that match " + flag + ": " + filteredFeatureFlags.toString());

    // verify we have a valid result
    if (filteredFeatureFlags.size() < 1) {
      throw new FlagNotFoundException(flag);
    }
    if (filteredFeatureFlags.size() > 1) {
      throw new FlagNotUniqueException(flag);
    }
    if (!filteredFeatureFlags.get(0).isEnabled()){
      throw new FlagDisabledException(flag);
    }
    int id = filteredFeatureFlags.get(0).getId();

    // evaluate the flag
    FeatureFlagPostEntity featureFlagPostEntity = new FeatureFlagPostEntity(id);
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<FeatureFlagPostEntity> entity = new HttpEntity<FeatureFlagPostEntity>(featureFlagPostEntity, headers);
    ResponseEntity<PostEvaluateFeatureFlagResponse> evaluationResponse = restTemplate.postForEntity(this.URL_FEATURE_FLAGS + this.FEATURE_FLAGS_EVALUATION, entity, PostEvaluateFeatureFlagResponse.class);
    PostEvaluateFeatureFlagResponse postEvaluateFeatureFlagResponse = evaluationResponse.getBody();
    log.debug("returning successful: " + postEvaluateFeatureFlagResponse.toString());
    return postEvaluateFeatureFlagResponse;
  }
}
