package nl.inquisitive.mosar.flashcard;

import com.zaxxer.hikari.HikariDataSource;
import nl.inquisitive.mosar.flashcard.security.SslParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.annotation.Profile;
import org.modelmapper.ModelMapper;

import javax.annotation.PostConstruct;
import java.io.File;

@SpringBootApplication
@ComponentScan(basePackages = "nl.inquisitive.mosar.flashcard")
public class FlashcardApplication {

  @Autowired
  SslParams sslParams;

  public static void main(String[] args) {
    SpringApplication.run(FlashcardApplication.class, args);
  }

  @Profile("aws")
  @Bean
  @ConfigurationProperties(prefix = "spring.datasource")
  public HikariDataSource dataSource() {
    HikariDataSource ds = DataSourceBuilder.create()
        .type(HikariDataSource.class)
        .build();
    return ds;
  }

  @PostConstruct
  void postConstruct() {

    // set TrustStoreParams
    File trustStoreFilePath = new File(sslParams.trustStorePath);
    String tsp = trustStoreFilePath.getAbsolutePath();
    System.setProperty("javax.net.ssl.trustStore", tsp);
    System.setProperty("javax.net.ssl.trustStorePassword", sslParams.trustStorePassword);
    System.setProperty("javax.net.ssl.keyStoreType", sslParams.defaultType);
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }

  @Bean
   public RestTemplate getRestTemplate() {
      return new RestTemplate();
   }
}
