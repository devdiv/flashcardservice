package nl.inquisitive.mosar.flashcard.exceptions.featureflag;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FlagNotFoundException
        extends RuntimeException {

    public FlagNotFoundException(String flag) {
        super("Feature flag " + flag + " was not found");
    }
}
