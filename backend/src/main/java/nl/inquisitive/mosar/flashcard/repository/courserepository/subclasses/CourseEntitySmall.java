package nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses;

import nl.inquisitive.mosar.flashcard.repository.courserepository.AbstractCourseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "course_entity")
public class CourseEntitySmall extends AbstractCourseEntity implements Serializable {

}
