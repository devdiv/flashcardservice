package nl.inquisitive.mosar.flashcard.models;

import java.util.List;
import lombok.Data;

@Data
public class GetAllFeatureFlagsResponse {
  private boolean dataRecordsEnabled;
  private String description;
  private boolean enabled;
  private int id;
  private String key;
  private List<String> segments;
  private List<String> tags;
  private String updatedAt;
  private List<String> variants;
}
