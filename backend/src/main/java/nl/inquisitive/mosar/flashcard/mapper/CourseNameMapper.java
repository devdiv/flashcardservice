//package nl.inquisitive.mosar.flashcard.mapper;
//
//import nl.inquisitive.mosar.flashcard.controller.model.CoursesAvailable;
//import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntity;
//import org.mapstruct.InheritInverseConfiguration;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.Mappings;
//import org.mapstruct.factory.Mappers;
//
//@Mapper
//public interface CourseNameMapper {
//
//    CourseNameMapper INSTANCE = Mappers.getMapper(CourseNameMapper.class);
//
//    //Mapp the data model from the open API specs on the database specs
//    @Mappings({
//            @Mapping(target = "courseid", source = "courseid"),
//            @Mapping(target = "coursenumber", source = "coursenumber"),
//            @Mapping(target = "coursename", source = "coursename"),
//
//    })
//    CourseEntity mapTo(CoursesAvailable coursesAvailable);
//
//    @InheritInverseConfiguration
//    CoursesAvailable mapTo(CourseEntity courseEntity);
//}
