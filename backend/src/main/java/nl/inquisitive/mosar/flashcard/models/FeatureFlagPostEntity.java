package nl.inquisitive.mosar.flashcard.models;

import java.util.HashMap;
import lombok.Data;

@Data
public class FeatureFlagPostEntity {
  private String entityId;
  private String entityType;
  private HashMap entityContext;
  private boolean enableDebug;
  private int flagId;
  private String flagKey;

  public FeatureFlagPostEntity(int id) {
    super();
    this.flagId = id;
  }

}
