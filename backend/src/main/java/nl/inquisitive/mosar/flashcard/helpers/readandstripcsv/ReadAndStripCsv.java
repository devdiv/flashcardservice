package nl.inquisitive.mosar.flashcard.helpers.readandstripcsv;

import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadAndStripCsv {

    @Setter
    @Getter
    List<String> allTermsList;

    @Getter
    @Setter
    List<String> allExplanationsList;

    public ReadAndStripCsv(String csvFileName){
        ArrayList<String> internalAllTermsList = new ArrayList<>();
        ArrayList<String> internalAllExplanationsList = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFileName))) {
            String line;
            boolean skipTheFirstLine = false;
            while ((line = br.readLine()) != null) {
                line = line.replaceAll("\"","");
                String kept = line.substring( 0, line.indexOf(","));
                String remainder = line.substring(line.indexOf(",")+1, line.length());
                if(skipTheFirstLine) {
                    internalAllTermsList.add(kept);
                    internalAllExplanationsList.add(remainder);
                }
                skipTheFirstLine= true;
            }
            setAllTermsList(internalAllTermsList);
            setAllExplanationsList(internalAllExplanationsList);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
