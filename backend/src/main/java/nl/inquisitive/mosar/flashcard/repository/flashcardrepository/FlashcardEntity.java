package nl.inquisitive.mosar.flashcard.repository.flashcardrepository;

import lombok.Getter;
import lombok.Setter;
import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntitySmall;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "flashcard_entity")
public class FlashcardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "courseid", referencedColumnName = "courseid", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private CourseEntitySmall courseEntitySmall;

    @Getter
    @Setter
    @Column
    private String flashcardid;

    @Getter
    @Setter
    @Lob
    @Column
    private String term;

    @Getter
    @Setter
    @Lob
    @Column
    private String explanation;

    @Getter
    @Setter
    private String createdate;

    @Getter
    @Setter
    private String createdby;

    @Getter
    @Setter
    private String changedby;
}
