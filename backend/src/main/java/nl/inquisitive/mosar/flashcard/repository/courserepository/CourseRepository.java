package nl.inquisitive.mosar.flashcard.repository.courserepository;

import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, Long> {
}
