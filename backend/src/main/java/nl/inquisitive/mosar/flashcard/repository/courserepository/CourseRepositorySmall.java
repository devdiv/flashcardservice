package nl.inquisitive.mosar.flashcard.repository.courserepository;

import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntitySmall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepositorySmall extends JpaRepository<CourseEntitySmall, Long> {

    CourseEntitySmall findByCourseid(long courseid);
}
