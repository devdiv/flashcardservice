package nl.inquisitive.mosar.flashcard.helpers.readandstripjson;

import com.google.gson.Gson;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.Model.ExamModel;

import java.io.File;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Files;

@Slf4j
@Data
public class ReadPSMJson {

    private Reader jsonReader;

    @Getter
    @Setter
    ExamModel examModel;

    public ReadPSMJson(URI jsonFileName) {
        convertFromFile(jsonFileName);
        log.debug("created object");
    }

    public void convertFromFile(URI jsonFileName) {
        try {
            File jsonToBeRead =  new File(jsonFileName);
            jsonReader = Files.newBufferedReader(jsonToBeRead.toPath());
            this.examModel = new Gson().fromJson(jsonReader, ExamModel.class);

        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }




}
