package nl.inquisitive.mosar.flashcard.helpers.readandstripjson.Model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

@Getter
@Setter
public class QuestionModel {
    private int questionNumberInExam;
    private int id;
    private String question;
    private String prereq;
    private HashMap<String, String> answers;
    private List<String> correctAnswer; //["a", "b"]
    private LinkedHashMap explanation;
    private List<String> imageFileNames;
    private List<String> answerGiven;
    private boolean answerIsCorrect;
    private boolean flagged;
}

