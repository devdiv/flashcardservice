package nl.inquisitive.mosar.flashcard.exceptions;

import java.util.Date;
import lombok.ToString;
import lombok.Getter;

@ToString
public class ErrorDetails {
  @Getter
  private Date timestamp;
  @Getter
  private String message;
  @Getter
  private String details;

  public ErrorDetails(Date timestamp, String message, String details) {
    this.timestamp = timestamp;
    this.message = message;
    this.details = details;
  }
}
