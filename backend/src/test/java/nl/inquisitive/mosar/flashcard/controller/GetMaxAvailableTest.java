package nl.inquisitive.mosar.flashcard.controller;

import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
import nl.inquisitive.mosar.flashcard.controller.model.Maxflashcards;
import nl.inquisitive.mosar.flashcard.service.FlashcardServiceLayer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

@SpringBootTest(classes = FlashcardController.class) // see https://stackoverflow.com/questions/72000835/mock-value-in-springboot-unit-test-not-working/72002073#72002073

public class GetMaxAvailableTest {

    @MockBean
    FlashcardServiceLayer flashcardServiceLayer;

    @MockBean
    ModelMapper modelMapper;

    @Autowired
    FlashcardController flashcardController;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getMaxAvailable_GivesResponseCodeAndMaxFlashcards_IfCourseIdExists() {
        //Setup Data
        Maxflashcards maxflashcards = new Maxflashcards();
        maxflashcards.setMaxFlashcards(33);

        //setup response
        Mockito.when(flashcardServiceLayer.getMaxValueFromDatabase("1")).thenReturn(maxflashcards);

        //Start testing class
        ResponseEntity<Maxflashcards> getMaxflashcardsResponse = flashcardController.getMaxAvailable("1");

        //Assert
        Assertions.assertEquals("200 OK",getMaxflashcardsResponse.getStatusCode().toString());
        Assertions.assertEquals("33",getMaxflashcardsResponse.getBody().getMaxFlashcards().toString());
    }

}
