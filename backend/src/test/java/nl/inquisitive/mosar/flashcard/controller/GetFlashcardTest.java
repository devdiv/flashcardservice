package nl.inquisitive.mosar.flashcard.controller;


import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
import nl.inquisitive.mosar.flashcard.service.FlashcardServiceLayer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

@SpringBootTest(classes = FlashcardController.class) // see https://stackoverflow.com/questions/72000835/mock-value-in-springboot-unit-test-not-working/72002073#72002073


public class GetFlashcardTest {

    @MockBean
    FlashcardServiceLayer flashcardServiceLayer;

    @MockBean
    ModelMapper modelMapper;

    @Autowired
    FlashcardController flashcardController;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getFlashcard_GivesResponseCodeAndTermAndExplanation_IfCourseIdAndFlashcardIdExists() {
        //Setup Data
        Flashcard flashcard = new Flashcard();
        flashcard.setExplanation("Explanation");
        flashcard.setTerm("Term");
        flashcard.setCourseid(Collections.singletonList("1"));
        flashcard.setFlashcardid("2");
        int id = 1;

        //setup response
        Mockito.when(flashcardServiceLayer.getAFlashcardByCourseIdAndFlashcardId("1","2")).thenReturn(flashcard);

        //Start testing class
        ResponseEntity<Flashcard> getFlashcardResponse = flashcardController.getFlashcard("1","2");

        //Assert
        Assertions.assertEquals("200 OK",getFlashcardResponse.getStatusCode().toString());
        Assertions.assertEquals("Term",getFlashcardResponse.getBody().getTerm());
        Assertions.assertEquals("Explanation",getFlashcardResponse.getBody().getExplanation());
    }
}

