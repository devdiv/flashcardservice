package nl.inquisitive.mosar.flashcard.service;

import nl.inquisitive.mosar.flashcard.models.FeatureFlagPostEntity;
import nl.inquisitive.mosar.flashcard.models.GetAllFeatureFlagsResponse;
import nl.inquisitive.mosar.flashcard.models.PostEvaluateFeatureFlagResponse;
import nl.inquisitive.mosar.flashcard.exceptions.featureflag.FlagDisabledException;
import nl.inquisitive.mosar.flashcard.exceptions.featureflag.FlagNotUniqueException;
import nl.inquisitive.mosar.flashcard.exceptions.featureflag.FlagNotFoundException;


import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(classes = FeatureFlagService.class) // see https://stackoverflow.com/questions/72000835/mock-value-in-springboot-unit-test-not-working/72002073#72002073
@TestPropertySource(properties = { "url.feature_flags = http://endpoint" })
class FeatureFlagServiceTests {
  HttpEntity<FeatureFlagPostEntity> entity;
  PostEvaluateFeatureFlagResponse mockEvaluationResponse;

  @MockBean
  RestTemplate restTemplate;

  @Autowired
  FeatureFlagService featureFlagService;

  @BeforeEach
  public void setUp(){

    GetAllFeatureFlagsResponse[] mockGetAllFeatureFlagsResponses = new GetAllFeatureFlagsResponse[] {new GetAllFeatureFlagsResponse(), new GetAllFeatureFlagsResponse(), new GetAllFeatureFlagsResponse(), new GetAllFeatureFlagsResponse(), new GetAllFeatureFlagsResponse()};
    mockGetAllFeatureFlagsResponses[0].setDescription("pizzaMargherita");
    mockGetAllFeatureFlagsResponses[0].setId(100);
    mockGetAllFeatureFlagsResponses[0].setEnabled(true);
    mockGetAllFeatureFlagsResponses[1].setDescription("pizzaHawaii");
    mockGetAllFeatureFlagsResponses[1].setId(42);
    mockGetAllFeatureFlagsResponses[1].setEnabled(true);
    mockGetAllFeatureFlagsResponses[2].setDescription("pizzaQuattroStagione");
    mockGetAllFeatureFlagsResponses[2].setId(21);
    mockGetAllFeatureFlagsResponses[2].setEnabled(false);
    mockGetAllFeatureFlagsResponses[3].setDescription("pizzaQuattroFormaggi");
    mockGetAllFeatureFlagsResponses[3].setId(22);
    mockGetAllFeatureFlagsResponses[3].setEnabled(false);
    mockGetAllFeatureFlagsResponses[4].setDescription("pizzaQuattroFormaggi");
    mockGetAllFeatureFlagsResponses[4].setId(23);
    mockGetAllFeatureFlagsResponses[4].setEnabled(true);

    // get All feature flags
    ResponseEntity mockGetAllFeatureFlagsResponseEntity = new ResponseEntity(mockGetAllFeatureFlagsResponses, null, 200);
    Mockito.when(restTemplate.getForEntity("http://endpoint/api/v1/flags", GetAllFeatureFlagsResponse[].class)).thenReturn(mockGetAllFeatureFlagsResponseEntity);

    // evaluate valid flag
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    FeatureFlagPostEntity featureFlagPostEntity = new FeatureFlagPostEntity(42);
    this.entity = new HttpEntity<FeatureFlagPostEntity>(featureFlagPostEntity, headers);
    this.mockEvaluationResponse = new PostEvaluateFeatureFlagResponse();
    ResponseEntity<PostEvaluateFeatureFlagResponse> mockPostEvaluateFeatureFlagsResponseEntity = new ResponseEntity(this.mockEvaluationResponse, null, 200);
    Mockito.when(restTemplate.postForEntity("http://endpoint/api/v1/evaluation", entity , PostEvaluateFeatureFlagResponse.class)).thenReturn(mockPostEvaluateFeatureFlagsResponseEntity);
  }

  @Test
  void serviceExists() {
    Assertions.assertNotNull(featureFlagService);
  }


  @Test
  void evaluateFeatureFlagCallsGetForEntityOnce() {
    featureFlagService.evaluateFeatureFlag("pizzaHawaii");
    Mockito.verify(restTemplate, Mockito.times(1)).getForEntity("http://endpoint/api/v1/flags", GetAllFeatureFlagsResponse[].class);
  }

  @Test
  void evaluateFeatureFlagCallsTheEvaluateFeatureFlagEndpointWithTheCorrectIdIfItExists(){
    featureFlagService.evaluateFeatureFlag("pizzaHawaii");
    Mockito.verify(restTemplate, Mockito.times(1)).postForEntity("http://endpoint/api/v1/evaluation", this.entity , PostEvaluateFeatureFlagResponse.class);
  }

  @Test
  void evaluateFeatureFlagReturnsPostEvaluateFeatureFlagResponseIfItExists(){
    PostEvaluateFeatureFlagResponse response  = featureFlagService.evaluateFeatureFlag("pizzaHawaii");
    Assertions.assertEquals(response, this.mockEvaluationResponse);
  }

  @Test
  void evaluateFeatureFlagThrowsFlagNotFoundExceptionIfRequestedFlagDoesNotExist() {
    Assertions.assertThrows(FlagNotFoundException.class, () -> featureFlagService.evaluateFeatureFlag("pizzaFunghi"));
  }

  @Test
  void evaluateFeatureFlagThrowsFlagDisabledExceptionIfRequestedFlagIsDisabled() {
    Assertions.assertThrows(FlagDisabledException.class, () -> featureFlagService.evaluateFeatureFlag("pizzaQuattroStagione"));
  }

  @Test
  void evaluateFeatureFlagThrowsFlagNotUniqueExceptionIfRequestedFlagDescriptionIsNotUnique() {
    Assertions.assertThrows(FlagNotUniqueException.class, () -> featureFlagService.evaluateFeatureFlag("pizzaQuattroFormaggi"));
  }
}
