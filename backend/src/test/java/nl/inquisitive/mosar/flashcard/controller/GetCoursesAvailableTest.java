package nl.inquisitive.mosar.flashcard.controller;

import nl.inquisitive.mosar.flashcard.controller.model.Course;
import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntity;
import nl.inquisitive.mosar.flashcard.repository.courserepository.subclasses.CourseEntitySmall;

import nl.inquisitive.mosar.flashcard.service.FlashcardServiceLayer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest(classes = FlashcardController.class) // see https://stackoverflow.com/questions/72000835/mock-value-in-springboot-unit-test-not-working/72002073#72002073

@ExtendWith(SpringExtension.class)
@Import(value = {ModelMapper.class})

public class GetCoursesAvailableTest {

    @MockBean
    FlashcardServiceLayer flashcardServiceLayer;

    @Autowired
    ModelMapper modelMapper;

    @MockBean
    CourseEntity courseEntity;

    @Autowired
    FlashcardController flashcardController;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCourseAvailable_GivesResponseCodeAndAllAvailableCourses_IfCourseIdExists() {
        //Setup Data


        CourseEntitySmall course1 = new CourseEntitySmall();
        course1.setCoursename("ISTQB");
        course1.setCourseid(1);
        CourseEntitySmall course2 = new CourseEntitySmall();
        course2.setCoursename("PSM1");
        course2.setCourseid(2);
        CourseEntitySmall course3 = new CourseEntitySmall();
        course3.setCoursename("DATAOPS");
        course3.setCourseid(3);

        List<CourseEntitySmall> courses = Arrays.asList(course1,course2,course3);
        //setup response
        Mockito.when(flashcardServiceLayer.listOfAllAvailableCourses()).thenReturn(courses);

        //Start testing class
        ResponseEntity<List<Course>> getAllAvailableCoursesResponse = flashcardController.getCoursesAvailable();

        //Assert
        Assertions.assertEquals("200 OK",getAllAvailableCoursesResponse.getStatusCode().toString());
        Assertions.assertEquals(3,getAllAvailableCoursesResponse.getBody().size());
        Assertions.assertEquals(1,getAllAvailableCoursesResponse.getBody().get(0).getCourseid());
        Assertions.assertEquals(2,getAllAvailableCoursesResponse.getBody().get(1).getCourseid());
        Assertions.assertEquals(3,getAllAvailableCoursesResponse.getBody().get(2).getCourseid());
        Assertions.assertEquals("ISTQB",getAllAvailableCoursesResponse.getBody().get(0).getCoursename());
        Assertions.assertEquals("PSM1",getAllAvailableCoursesResponse.getBody().get(1).getCoursename());
        Assertions.assertEquals("DATAOPS",getAllAvailableCoursesResponse.getBody().get(2).getCoursename());

    }
}
