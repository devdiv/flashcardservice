FROM openjdk:11.0.11-jdk-slim

# We need wget in start-flashcards which is run in aws by ecs task
RUN  apt-get update \
  && apt-get install -y wget

WORKDIR /app
COPY target/flashcardservice*SNAPSHOT.jar ./target/flashcards.jar
COPY start-flashcards.sh .
ENV SPRING_PROFILES_ACTIVE=aws
ENV DB_HOST=bla_db_host
ENV DB_USER=bla_db_user
ENV DATABASE=bla_database
ENV TRUSTSTORE_PASS=bla_truststore_pass
CMD ["java", "-jar", "target/flashcards.jar"]

