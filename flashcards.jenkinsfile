pipeline {
  agent none
  environment {
    KANIKO_SUBNETS            = "${SUBNET_IDS}".split(",")
    KANIKO_SUBNET_1           = "${KANIKO_SUBNETS[0]}"
    KANIKO_SUBNET_2           = "${KANIKO_SUBNETS[1]}"
    KANIKO_SUBNET_3           = "${KANIKO_SUBNETS[2]}"
    AWS_TF_BUCKET             = "terraform-state-flashcard-${ENVIRONMENT}-2tt4"
    LOCK_TABLE_FLASHCARD_PROD = "${LOCK_TABLE_FLASHCARD_PROD}"
    LOCK_TABLE_FLASHCARD_TEST = "${LOCK_TABLE_FLASHCARD_TEST}"
    BUCKET_FLASHCARD          = "${BUCKET_FLASHCARD}"
    SPRING_PROFILES_ACTIVE    = "${SPRING_PROFILES_ACTIVE}"
  }
  post {
    failure {
      updateGitlabCommitStatus name: 'jenkins-flashcard', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'jenkins-flashcard', state: 'success'
    }
  }
  options {
    gitLabConnection('devidiv-gitlab-connection')
    gitlabBuilds(builds: ['terraform-check-test','terraform-validate-test','terraform-plan-test','flashcard-test','terraform-check-prod','terraform-validate-prod','terraform-plan-prod', 'flashcard-build-and-publish'])
  }

  stages {
    stage('parallel'){
      parallel {
          stage ('terraform-check-and-plan-test') {
            environment {
              LOCK_TABLE_FLASHCARD = "${LOCK_TABLE_FLASHCARD_TEST}"
                ENVIRONMENT        = "test"
                TF_VAR_ENVIRONMENT = "test"
            }
            agent {
              label 'mosar_terraform'
            }
            stages {
              stage('terraform-check-test') {
                  steps {
                    sh "mkdir -p ./backend/tf && cp -r ./backend/terraform ./backend/tf/${ENVIRONMENT}"
                    sh "cd ./backend/tf/${ENVIRONMENT} && envsubst < providers-template.tf > providers.tf && rm providers-template.tf"
                    sh "cd ./backend/tf/${ENVIRONMENT} && envsubst < terraform-template.tfvars > terraform.tfvars && rm terraform-template.tfvars"
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform fmt --check"
                  }
                  post {
                    success {
                      updateGitlabCommitStatus name: 'terraform-check-test', state: 'success'
                    }
                    failure {
                      updateGitlabCommitStatus name: 'terraform-check-test', state: 'failed'
                    }
                  }
                }
              stage('terraform-validate-test') {
                  steps {
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform init"
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform validate"
                  }
                  post {
                    success {
                      updateGitlabCommitStatus name: 'terraform-validate-test', state: 'success'
                      }
                      failure {
                        updateGitlabCommitStatus name: 'terraform-validate-test', state: 'failed'
                      }
                    }
                  }
                stage('terraform-plan-test') {
                  steps {
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform plan --out=${GIT_COMMIT}.tfplan"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/${GIT_COMMIT}.tfplan s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_plans/${GIT_COMMIT}.tfplan"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/${GIT_COMMIT}.tfplan s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_plans/latest.tfplan"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/terraform.tfvars s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars.new"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/providers.tf s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf.new"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/previous.tfvars || true"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/previous.providers.tf || true"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars.new s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf.new s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf"
                  }
                  post {
                    success {
                      updateGitlabCommitStatus name: 'terraform-plan-test', state: 'success'
                    }
                    failure {
                      updateGitlabCommitStatus name: 'terraform-plan-test', state: 'failed'
                    }
                  }
                }
              }
            }
          stage ('terraform-check-and-plan-prod') {
            environment {
              LOCK_TABLE_FLASHCARD = "${LOCK_TABLE_FLASHCARD_PROD}"
              ENVIRONMENT          = "prod"
              TF_VAR_ENVIRONMENT   = "prod"
            }
            agent {
              label 'mosar_terraform'
            }
            stages {
              stage('terraform-check-prod') {
                  steps {
                    sh "mkdir -p ./backend/tf && cp -r ./backend/terraform ./backend/tf/${ENVIRONMENT}"
                    sh "cd ./backend/tf/${ENVIRONMENT} && envsubst < providers-template.tf > providers.tf && rm providers-template.tf"
                    sh "cd ./backend/tf/${ENVIRONMENT} && envsubst < terraform-template.tfvars > terraform.tfvars && rm terraform-template.tfvars"
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform fmt --check"
                  }
                  post {
                    success {
                      updateGitlabCommitStatus name: 'terraform-check-prod', state: 'success'
                    }
                    failure {
                      updateGitlabCommitStatus name: 'terraform-check-prod', state: 'failed'
                    }
                  }
                }
              stage('terraform-validate-prod') {
                  steps {
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform init"
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform validate"
                  }
                  post {
                    success {
                      updateGitlabCommitStatus name: 'terraform-validate-prod', state: 'success'
                      }
                      failure {
                        updateGitlabCommitStatus name: 'terraform-validate-prod', state: 'failed'
                      }
                    }
                  }
                stage('terraform-plan-prod') {
                  steps {
                    sh "cd ./backend/tf/${ENVIRONMENT} && terraform plan --out=${GIT_COMMIT}.tfplan"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/${GIT_COMMIT}.tfplan s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_plans/${GIT_COMMIT}.tfplan"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/${GIT_COMMIT}.tfplan s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_plans/latest.tfplan"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/terraform.tfvars s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars.new"
                    sh "aws s3 cp ./backend/tf/${ENVIRONMENT}/providers.tf s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf.new"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/previous.tfvars || true"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/previous.providers.tf || true"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars.new s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/terraform.tfvars"
                    sh "aws s3 mv s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf.new s3://${BUCKET_FLASHCARD}/${ENVIRONMENT}/terraform_files/providers.tf"
                  }
                  post {
                    success {
                      updateGitlabCommitStatus name: 'terraform-plan-prod', state: 'success'
                    }
                    failure {
                      updateGitlabCommitStatus name: 'terraform-plan-prod', state: 'failed'
                    }
                  }
                }
              }
            }

          stage ('flashcard-test') {
            agent {
              label 'mosar_backend'
            }
            steps {
              sh 'cd ./backend && mvn --no-transfer-progress clean test'
            }
            post {
              success {
                updateGitlabCommitStatus name: 'flashcard-test', state: 'success'
              }
              failure {
                updateGitlabCommitStatus name: 'flashcard-test', state: 'failed'
              }
            }
          }
          stage ('flashcard-build-and-publish') {
            agent {
              label 'mosar_backend'
            }
            steps {
              sh 'cd ./backend && mvn --no-transfer-progress clean install'
              sh "tar c ./backend | gzip | aws s3 cp - 's3://${BUCKET_FLASHCARD}/kaniko-context/context.tar.gz'"
              echo 'Build docker image with Kaniko and publish to ECR'
              sh 'envsubst < kaniko-task-flashcard-template.json > ecs-run-task.json'
              script {
                  LATEST_TASK_DEFINITION = sh(returnStdout: true, script: "/bin/bash -c 'aws ecs list-task-definitions \
                      --status active --sort DESC \
                      --family-prefix $KANIKO_FLASHCARD_FAMILY \
                      --query \'taskDefinitionArns[0]\' \
                      --output text \
                      | sed \'s:.*/::\''").trim()
                  TASK_ARN = sh(returnStdout: true, script: "/bin/bash -c 'aws ecs run-task \
                      --task-definition $LATEST_TASK_DEFINITION \
                        --cli-input-json file://ecs-run-task.json \
                      | jq -j \'.tasks[0].taskArn\''").trim()
              }
              echo "Submitted task $TASK_ARN"

              sh "aws ecs wait tasks-running --cluster ${ECS_AGENT_CLUSTER} --task $TASK_ARN"
              echo "Task is running"

              sh "aws ecs wait tasks-stopped --cluster ${ECS_AGENT_CLUSTER} --task $TASK_ARN"
              echo "Task has stopped"
              script {
                  EXIT_CODE = sh(returnStdout: true, script: "/bin/bash -c 'aws ecs describe-tasks \
                  --cluster ${ECS_AGENT_CLUSTER} \
                  --tasks $TASK_ARN \
                  --query \'tasks[0].containers[0].exitCode\' \
                  --output text'").trim()

                  if (EXIT_CODE == '0') {
                      echo 'Successfully built and published Docker image'
                  }
                  else {
                      error("Container exited with unexpected exit code $EXIT_CODE. Check the logs for details.")
                  }
              }
            }
            post {
              success {
                updateGitlabCommitStatus name: 'flashcard-build-and-publish', state: 'success'
              }
              failure {
                updateGitlabCommitStatus name: 'flashcard-build-and-publish', state: 'failed'
              }
            }
          }
        }
      }
    }
  }
